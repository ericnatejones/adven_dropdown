<?php 

/*
Single Post Template: Author Page
Description: This is for the author profile page.
*/

?>  

<?php get_header(); ?>
	<div class="single-page-container">
            <div class="row">

                <div id="main-content" class="col-sm-8 single-author-page">

					<!-- <!– This sets the $curauth variable –> -->
					<?php
					if(isset($_GET['author_name'])) :
					$curauth = get_userdatabylogin($author_name);
						else :
						$curauth = get_userdata(intval($author));
					endif;
					?>
					<h2 class="author-name"><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></h2>
						<dl>
							<div class="author-image col-sm-3"><?php echo get_avatar( get_the_author_meta( 'ID' ), 250 ); ?></div>
							<div class="author-text col-sm-8">
								<p class="author-social">
			                        <a target="_blank" href="http://www.facebook.com/<?php echo $curauth->facebook; ?>"><i class="fa fa-facebook"></i>/<?php echo $curauth->facebook; ?></a>
			                        <a target="_blank" href="http://www.instagram.com/<?php echo $curauth->instagram; ?>"><i class="fa fa-instagram"></i>@<?php echo $curauth->instagram; ?></a>
			                        <a target="_blank" href="http://www.twitter.com/<?php echo $curauth->twitter; ?>"><i class="fa fa-twitter"></i>@<?php echo $curauth->twitter; ?></a>
			                    </p>
								<div><?php echo $curauth->user_description; ?></div>
							</div>
						</dl>
					<div class="related-posts-author">
						<h2>Posts by <?php the_author_firstname(); ?>:</h2>
						<!-- <!– The Loop –> -->
						<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
							<div class="author-loop">
								<a class="author-loop-image col-sm-6" href="<?php the_permalink(); ?>" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
                    			</a>
								<a class="author-loop-text col-sm-6" href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link: <?php the_title(); ?>">
								<?php the_title(); ?></a>
							</div>
						<?php endwhile; else: ?>
						<p><?php _e('No posts by this author.'); ?></p>
						<?php endif; ?>
						<!-- <!– End Loop –> -->
					</div>
				</div>
			<?php get_sidebar(); ?>
			</div>
		</div>

			<?php get_footer(); ?>