<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

    <?php if ( has_post_thumbnail() ) : ?>
    <article class="post category-loop">
        <div class=" loop-image-wrapper col-sm-6">

                   <?php
                if (has_post_video( get_the_id() )){

                    the_post_video();
                    
                } else { ?>


                    <a class="loop-image" href="<?php the_permalink(); ?>" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
                    </a>

                    <?php }  ?>
        </div>

        <div class="loop-text col-sm-6">
            <header>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <div class="date-author"><?php display_categorys_except_featured() ?>   •    <?php the_time('F j, Y'); ?>   •    <?php the_author_link(); ?></div>
                <span class="time-span"><i class="fa fa-clock-o"></i><?php echo get_time_to_read(get_the_ID()); ?></span>
            </header>
        </div>

    </article>
    <?php endif; ?>

    <?php endwhile; else: ?>
    <p class="no-posts"><?php _e('No posts were found. Sorry!'); ?></p>
    <?php endif; ?>
