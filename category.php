        <?php get_header(); ?>

        <div class="category-container">
            <div class="row">

                <div id="main-content" class="col-sm-12 lookbook">

                    <?php

                        global $query_string;
                        parse_str( $query_string, $args );
                        $args['posts_per_page'] = 32;
                        query_posts($args);

                        $current_cat = get_category(get_query_var('cat'));


                  ?>
                    <h1 id="page-title" class="category-title"><?php single_cat_title(); ?></h1>

                    <?php if ($current_cat->slug == 'looks'){?>
                      <div class="background-category-wrapper col-sm-6">
                        <h1 id="page-title" class="picture-category-title"><?php single_cat_title(); ?></h1>
                        <div class="category-description"><?php echo category_description( $category_id ); ?></div>
                      </div>
                      <div class="category-image">
                        <?php if ( has_post_thumbnail() ) :
                            $feat_image_url = cfi_featured_image_url();
                        ?>
                        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cfi_feat_image_url; ?>)">
                        </div>
                          <?php endif; ?>
                      </div>

                    <?php

                  } elseif ($current_cat->slug == 'beauty'){?>
                    <div class="background-category-wrapper col-sm-6">
                      <h1 id="page-title" class="picture-category-title"><?php single_cat_title(); ?></h1>
                      <div class="category-description"><?php echo category_description( $category_id ); ?></div>
                    </div>
                       <div class="category-image">
                        <?php if ( has_post_thumbnail() ) :
                            $feat_image_url = cfi_featured_image_url();
                        ?>
                        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cfi_feat_image_url; ?>)">
                        </div>
                          <?php endif; ?>
                    </div>


                  <?php } ?>

                    <?php include "category-loop.php"; ?>

                </div>

            </div>
        </div>

        <?php get_footer(); ?>
