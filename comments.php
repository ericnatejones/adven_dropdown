<?php

// Do not delete these lines
    if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
        die ('Please do not load this page directly. Thanks!');

    if ( post_password_required() ) { ?>
        <p class="nocomments"><?php _e('This post is password protected. Enter the password to view comments.'); ?></p>
    <?php
        return;
    }
?>

<!-- You can start editing here. -->

<?php if ( comments_open() ) : ?>

<div id="respond">

<input id="show-comment-<?php the_ID(); ?>" class="show-comment" type="checkbox" <?php if(isset($_GET['replytocom'])){ echo "checked";}; ?> >
<label id="show-comment-button" for="show-comment-<?php the_ID(); ?>" class="button show-comment-button">Leave a Comment +</label>
<div class="comment-form">
    <h3><?php comment_form_title( __('What do you think?'), __('Leave a Reply to %s' ) ); ?></h3>

    <div id="cancel-comment-reply">
        <small><?php cancel_comment_reply_link() ?></small>
    </div>

    <?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
    <p><?php printf(__('You must be <a href="%s">logged in</a> to post a comment.'), wp_login_url( get_permalink() )); ?></p>
    <?php else : ?>

    <form action="<?php echo site_url(); ?>/wp-comments-post.php" method="post" id="commentform">

    <?php if ( is_user_logged_in() ) : ?>

    <p><?php printf(__('Logged in as <strong><em><a href="%1$s">%2$s</a></em></strong>.'), get_edit_user_link(), $user_identity); ?> <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php esc_attr_e('Log out of this account'); ?>"><?php _e('Log out &raquo;'); ?></a></p>

    <?php else : ?>

    <p class="half half-left">
    <input type="text" placeholder="NAME *" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" size="22" tabindex="1" <?php if ($req) echo "aria-required='true'"; ?> /></p>

    <p class="half half-right">
    <input type="text" placeholder="EMAIL *" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" size="22" tabindex="2" <?php if ($req) echo "aria-required='true'"; ?> /></p>

    <p><input type="text" placeholder="WEBSITE" name="url" id="url" value="<?php echo  esc_attr($comment_author_url); ?>" size="22" tabindex="3" /></p>

    <?php endif; ?>

    <!--<p><small><?php printf(__('<strong>XHTML:</strong> You can use these tags: <code>%s</code>'), allowed_tags()); ?></small></p>-->

    <textarea name="comment" placeholder="YOUR COMMENT" id="comment" cols="58" rows="10" tabindex="4"></textarea>
    <input name="submit" type="submit" id="submit" tabindex="5" value="<?php esc_attr_e('Send'); ?>" />
    <?php comment_id_fields(); ?>
    <?php do_action('comment_form', $post->ID); ?>

    </form>

</div>

<?php endif; // If registration required and not logged in ?>
</div>

<?php endif; // if you delete this the sky will fall on your head ?>

<?php if ( have_comments()  && !is_front_page() ) : ?>

    <div class="navigation">
        <div class="alignleft"><?php previous_comments_link() ?></div>
        <div class="alignright"><?php next_comments_link() ?></div>
    </div>

    <ol class="commentlist">
    <?php wp_list_comments(array('avatar_size' => 50));?>
    </ol>

    <div class="navigation">
        <div class="alignleft"><?php previous_comments_link() ?></div>
        <div class="alignright"><?php next_comments_link() ?></div>
    </div>
 <?php else : // this is displayed if there are no comments so far ?>

    <?php if ( comments_open() ) : ?>
        <!-- If comments are open, but there are no comments. -->

     <?php else : // comments are closed ?>
        <!-- If comments are closed. -->
        <p class="nocomments"><?php _e('Comments are closed.'); ?></p>

    <?php endif; ?>
<?php endif; ?>


