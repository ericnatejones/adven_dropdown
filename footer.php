</section>

<div id="before-footer">
	<?php if ( !function_exists('dynamic_sidebar') ||
	!dynamic_sidebar('Before Footer') ) :
	 endif; ?>
</div>

<div class="page-divider">
    <hr>
    <img class="page-divider-image" src='<?php echo IMAGES; ?>/page-divider.png'>
</div>

<section id="instagram-footer">
<?php echo do_shortcode('[instagram-feed]'); ?>
</section>

<footer id="main-footer" class="container-fluid">
    <div class="row">
    	<div class="footer-menu">
        <?php
            $defaults = array(
                'container'       => 'nav',
                'container_id'    => 'footer-menu-container',
                'menu_id'         => 'footer-menu',
                'theme_location'  => 'footer'
            );

            wp_nav_menu( $defaults );
        ?>
        </div>
        <p> <a href="#" class="scrollToTop">Scroll To Top</a> &copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> All Rights Reserved. Designed and Developed by <a target="_blank" href="http://verisage.us/en/blog-design/">Verisage</a></p>
    </div>
</footer>

<aside id="newsletter-modal" class="modal">
    <div id="newsletter-modal-box" class="modal-box">
        <div id="newsletter-modal-inner" class="modal-inner">
            <a href="#" id="newsletter-close"><i class="fa fa-times"></i></a>
            <p>Want to get all the cool perks that come with being an adventurer? Sign up now and get the skim on what's new, event creation & more!</p>
            <footer id="newsletter-footer">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                <form action="//adventurself.us12.list-manage.com/subscribe/post?u=855c48b64509d4385c433558e&amp;id=024555831a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <div id="mc_embed_signup_scroll">
                <div class="mc-field-group">
                    <input type="text" placeholder="FIRST NAME" value="" name="FNAME" class="" id="mce-FNAME">
                </div>
                <div class="mc-field-group">
                    <input type="text" placeholder="LAST NAME" value="" name="LNAME" class="" id="mce-LNAME">
                </div>
                <div class="mc-field-group">
                    <input type="email" placeholder="EMAIL *" value="" name="EMAIL" class="required email" id="mce-EMAIL">
                </div>
                    <div id="mce-responses" class="clear">
                        <div class="response" id="mce-error-response" style="display:none"></div>
                        <div class="response" id="mce-success-response" style="display:none"></div>
                    </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                    <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_58f4e739c75c19b76b5c7997e_920c498a5f" tabindex="-1" value=""></div>
                 <div class="clear"><input type="submit" value=">" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                    </div>
                </form>
                </div>
                <!--End mc_embed_signup-->
            </footer>
        </div>
    </div>

</aside>

<?php wp_footer(); ?>

</body>
</html>
