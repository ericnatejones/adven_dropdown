
  <?php

      // global $query_string;
      // parse_str( $query_string, $args );
      // query_posts($args);
      //
      // $grab_cat = get_category(get_query_var('cat'));
      // $category_id = $grab_cat->term_id;

?>
<?php
$cat_id = get_cat_ID( 'heart' );

?>
  <a href="<?php echo esc_url(get_category_link( $cat_id )); ?> ">
    <div class="category-wrapper">
      <h1 class="picture-category-title"><?php echo get_cat_name($cat_id); ?></h1>
      <div class="category-image">
        <?php
            $cat_id = get_cat_ID( 'heart' );

            $cat_image_url = cfi_featured_image_url( array( 'size' => 'large', 'cat_id' => $cat_id ) );
        ?>
        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cat_image_url; ?>)">
        </div>
      </div>
    </div>
  </a>

  <?php
$cat_id = get_cat_ID( 'mind' );
?>
<a href="<?php echo esc_url(get_category_link( $cat_id )); ?> ">
    <div class="category-wrapper">
      <h1 class="picture-category-title"><?php echo get_cat_name($cat_id); ?></h1>
      <div class="category-image">
        <?php
            $cat_id = get_cat_ID( 'mind' );

            $cat_image_url = cfi_featured_image_url( array( 'size' => 'large', 'cat_id' => $cat_id ) );
        ?>
        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cat_image_url; ?>)"></div>
      </div>
    </div>
</a>

<?php
$cat_id = get_cat_ID( 'body' );
?>
<a href="<?php echo esc_url(get_category_link( $cat_id )); ?> ">

    <div class="category-wrapper">
      <h1 class="picture-category-title"><?php echo get_cat_name($cat_id); ?></h1>
      <div class="category-image">
        <?php
            $cat_id = get_cat_ID( 'body' );

            $cat_image_url = cfi_featured_image_url( array( 'size' => 'large', 'cat_id' => $cat_id ) );
        ?>
        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cat_image_url; ?>)">
        </div>
      </div>
    </div>
</a>


<?php
$cat_id = get_cat_ID( 'soul' );
?>

<a href="<?php echo esc_url(get_category_link( $cat_id )); ?> ">
    <div class="category-wrapper">
      <h1 class="picture-category-title"><?php echo get_cat_name($cat_id); ?></h1>
      <div class="category-image">
        <?php
            $cat_id = get_cat_ID( 'soul' );

            $cat_image_url = cfi_featured_image_url( array( 'size' => 'large', 'cat_id' => $cat_id ) );
        ?>
        <div class="loop-image background-image" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $cat_image_url; ?>)">
        </div>
      </div>
    </div>
</a>
