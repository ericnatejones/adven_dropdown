    <?php get_header(); ?>
        <div class="page-container home">
            <div class="row">

                <div class="category-grid-wrapper">
                    <?php include "four-category-grid.php"; ?>
                </div>


                <div id="main-content" class="col-sm-8">

                    <?php include "loop.php"; ?>

                    <?php


                    $prev_link = get_previous_posts_link('Previous');
                    $next_link = get_next_posts_link('Next');

                    ?>

                </div>

                <?php get_sidebar(); ?>

            </div>

            <div class="pagination-links">
                <?php echo $prev_link; ?>
                <?php if ($prev_link && $next_link): ?>  <?php endif; ?>
                <?php echo $next_link; ?>
            </div>
        </div>

      <?php get_footer(); ?>
