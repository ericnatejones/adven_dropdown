<?php 

/**
 * Template Name: Full Page
 *
 */

get_header(); ?>

    <a class="page-background-image loop-image" href="<?php the_permalink(); ?>" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
    </a>

    <div class="page-page-container">
        <div class="row">

            <div id="main-content" class="col-sm-8">

                <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                <article class="post">
                    <header>
                        <h1 id="page-title"><?php the_title(); ?></h1>
                    </header>
                    <div class="post-body">
                        <?php the_content('Read More...'); ?>
                    </div>
                    
                </article>

                <?php endwhile; else: ?>
                <p><?php _e('No posts were found. Sorry!'); ?></p>
                <?php endif; ?>

            </div>

            <?php get_sidebar(); ?>

        </div>
    </div>

<?php get_footer(); ?>