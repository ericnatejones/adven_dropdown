<?php

define( 'THEMEPATH', get_bloginfo('stylesheet_directory'));
define( 'IMAGES', THEMEPATH. "/img");

/* ------- Line Break Shortcode --------*/
function line_break_shortcode() {
  return '<br />';
}
add_shortcode( 'br', 'line_break_shortcode' );
include(get_template_directory().'/includes/featured_slider.php');

/*-------- Display Categories except Featured -------*/
function display_categorys_except_featured() {
  $categories = get_the_category();

  foreach ($categories as $i => $category){
    if ($category->name === 'Featured'){
      unset($categories[$i]);
    }
  }

  $numItems = count($categories);
  $i = 0;
  foreach ($categories as $category){
    $i++;
    $category_name = urlencode($category->name);
    $category_name = str_replace("+","-",$category_name);
    echo '<a href="category/'.$category_name.'">'.$category->name.'</a>';

    if($i !== $numItems) {
      echo ", ";
    }
  }
}


/*----------- User Social Media on Contacts -------------- */
function my_new_contactmethods( $contactmethods ) {
//Add Facebook
$contactmethods['facebook'] = 'Facebook';
// Add Instagram
$contactmethods['instagram'] = 'Instagram';
// Add Twitter
$contactmethods['twitter'] = 'Twitter';

return $contactmethods;
}
add_filter('user_contactmethods','my_new_contactmethods',10,1);


/*---------- Location Category --------*/
//hook into the init action and call create_book_taxonomies when it fires
add_action( 'init', 'create_locations_hierarchical_taxonomy', 0 );

function create_locations_hierarchical_taxonomy() {
// Add new taxonomy, make it hierarchical like categories
//first do the translations part for GUI

  $labels = array(
    'name' => _x( 'Location', 'taxonomy general name' ),
    'singular_name' => _x( 'Location', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Locations' ),
    'all_items' => __( 'All Locations' ),
    'parent_item' => __( 'Parent Location' ),
    'parent_item_colon' => __( 'Parent Location:' ),
    'edit_item' => __( 'Edit Location' ),
    'update_item' => __( 'Update Location' ),
    'add_new_item' => __( 'Add New Location' ),
    'new_item_name' => __( 'New Location Name' ),
    'menu_name' => __( 'Locations' ),
  );

// Now register the taxonomy
  register_taxonomy('locations',array('post'), array(
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'show_admin_column' => true,
    'query_var' => true,
    'rewrite' => array( 'slug' => 'location' ),
  ));
  register_meta('term', 'latlong', 'term_sanitize');
}
function term_sanitize($value){
  return $value;
}
add_action('edit_locations', 'lookup_latlong');
add_action('create_locations', 'lookup_latlong');

function lookup_latlong($term_id){
  $api_key = 'AIzaSyAIIWMGhXVkw8NHYTKcadDLkVHhA-zBTIM';
  $location_obj = get_term($term_id, 'locations');

  $location = urlencode($location_obj->name);

  $geolocation_url = "https://maps.googleapis.com/maps/api/geocode/json?address=$location&key=$api_key";

  $geo_json = file_get_contents($geolocation_url);
  // var_dump($geo_json); die;
  $geocode_data = json_decode($geo_json);

  $lat = $geocode_data->results[0]->geometry->location->lat;
  $lng = $geocode_data->results[0]->geometry->location->lng;

  // var_dump($lat);
  // var_dump($lng); die;

  $latlng = "$lat,$lng";
  update_term_meta($term_id, 'latlong', $latlng );

};

function mapael_scripts(){
    wp_enqueue_script('jquery');
    wp_register_script(
      'raphael',
      THEMEPATH . '/js/mapael/raphael-min.js',
      array()
    );
    wp_enqueue_script('raphael');
    wp_register_script(
      'mapael',
      THEMEPATH . '/js/mapael/jquery.mapael.min.js',
      array('jquery', 'raphael')
    );
    wp_enqueue_script('mapael');
    wp_register_script(
      'world_countries',
      THEMEPATH . '/js/mapael/maps/world_countries.min.js',
      array('mapael')
    );
    wp_enqueue_script('world_countries');

}
add_action('wp_enqueue_scripts', 'mapael_scripts');

/*------ Facebook Likes ------*/
function facebook_count( $theglamandglitter ) {
    $facebook_count = file_get_contents( 'http://graph.facebook.com/'.$theglamandglitter );
    return json_decode( $facebook_count )->likes;
}

/* ------- Default Image no link ------ */
function wpb_imagelink_setup() {
    $image_set = get_option( 'image_default_link_type' );

    if ($image_set !== 'none') {
        update_option('image_default_link_type', 'none');
    }
}
add_action('admin_init', 'wpb_imagelink_setup', 10);

/*** TOP SLIDER ***/
add_theme_support( 'post-thumbnails' );

// add_action('init', 'create_slider_type');

function create_slider_type() {

    $myslidertype_args = array(

        'label' => __('Slider'),
        'singular_label' => __('Slider Image'),
        'public' => false,
        'show_ui' => true,
        'menu_position' => 5,
        'capability_type' => 'post',
        'supports' => array(
          'title',
          'post-thumbnails',
          'thumbnail'
      )

       );

  register_post_type('kj_slidertype',$myslidertype_args);

}

add_action( 'after_setup_theme', 'slider_theme_setup' );
function slider_theme_setup() {
  add_image_size( 'slider', 1440, false ); // (cropped)
  function custom_image_sizes($sizes){
    $custom_sizes = array(
      'slider' => 'Slider'   );
    return array_merge($sizes, $custom_sizes);
  }
  add_filter('image_size_names_choose', 'custom_image_sizes');
}

function register_featured_slideshow() {
    if (is_front_page()){
        wp_enqueue_script('jquery');

        wp_register_script(
          'images_loaded',
          THEMEPATH . '/js/imagesloaded.pkgd.min.js',
          array('jquery')
        );
        wp_enqueue_script('images_loaded');

        wp_register_script(
          'featured_slideshow',
           THEMEPATH . '/js/featured-slideshow.js',
           array('jquery', 'images_loaded')
        );
        wp_enqueue_script('featured_slideshow');

    }

}
add_action('wp_enqueue_scripts', 'register_featured_slideshow');

function register_insta_slideshow_script() {
    wp_enqueue_script('jquery');
    wp_register_script(
      'instagram_slideshow',
       THEMEPATH . '/js/ig-slideshow.js',
       array('jquery')
    );
    wp_enqueue_script('instagram_slideshow');
    wp_register_style( 'ig-slideshow-style', THEMEPATH . '/css/ig-slideshow.css' );
    wp_enqueue_style('ig-slideshow-style');
}
add_action('wp_enqueue_scripts', 'register_insta_slideshow_script');

function social_menu() {
  add_menu_page( 'Social Links', 'Social Links', 'manage_options', 'social-links', 'social_link_form');
}

function social_link_form() {
  include('social-menu.php');
}

add_action( 'admin_menu', 'social_menu');

/*** EMAIL FEED ***/

function extra_feeds() {
  add_feed('email', 'get_email_template');
  add_feed('rss2', 'get_noncaption_template');
  add_feed('rss', 'get_noncaption_template');
}
add_action('init', 'extra_feeds');

function get_email_template() {
  add_filter('the_content_feed', 'size_images_for_email');
  include(ABSPATH . '/wp-includes/feed-rss2.php');
}

function get_noncaption_template() {
    add_filter('the_content_feed', 'size_images_for_email');
    add_filter('the_content_feed', 'remove_captions');
    include(ABSPATH . '/wp-includes/feed-rss2.php');
}

function size_images_for_email($content) {
  $content = '<div>' . $content . '</div>';
  $doc = new DOMDocument();
  $doc->loadHTML('<?xml version="1.0" encoding="UTF-8"?>' . $content);
  $images = $doc->getElementsByTagName('img');
  foreach ($images as $image) {
    $image->removeAttribute('height');
    $image->removeAttribute('width');
    $image->setAttribute('style', 'width: 100%; max-width: 600px; height: auto;');
  }
  // Strip weird DOCTYPE that DOMDocument() adds in
  $content = substr($doc->saveXML($doc->getElementsByTagName('div')->item(0)), 5, -6);
  return $content;
}

function remove_captions($content) {
    $content = '<div>' . $content . '</div>';
    $doc = new DOMDocument();
    $doc->loadHTML('<?xml version="1.0" encoding="UTF-8"?>' . $content);
    $finder = new DOMXPath($doc);
    $classname = 'wp-caption-text';
    $nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' $classname ')]");

    foreach ($nodes as $node) {
        $node->parentNode->removeChild($node);
    }
    // Strip weird DOCTYPE that DOMDocument() adds in
    $content = substr($doc->saveXML($doc->getElementsByTagName('div')->item(0)), 5, -6);
    return $content;
}


/*** MENUS ***/
add_theme_support('nav-menus');
if (function_exists('register_nav_menus')) {
    register_nav_menus(
        array(
            'main' => 'Main Nav',
            'lookbook' => 'Lookbook',
            'footer' => 'Footer'
        )
    );
}

function register_nav_scripts() {
    wp_enqueue_script('jquery');

    wp_register_script(
      'nav_menu',
      THEMEPATH . '/js/nav-menu.js',
      array('jquery')
    );
    wp_enqueue_script('nav_menu');
}
add_action('wp_enqueue_scripts', 'register_nav_scripts');


/*** SIDEBAR ***/
if (function_exists('register_sidebar')) {
    register_sidebar( array(
        'name' => __('Primary Sidebar', 'primary-sidebar'),
        'id' => 'primary-widget-area',
        'description' => __('The primary widget area', 'dir'),
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<div class="widget-header"><span>',
        'after_title' => '</span></div>',
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar( array(
        'name' => __('Front Page Sidebar', 'front-page-sidebar'),
        'id' => 'front-page-widget-area',
        'description' => __('The front page widget area', 'dir'),
        'before_widget' => '<div class="widget">',
        'after_widget' => '</div>',
        'before_title' => '<div class="widget-header"><span>',
        'after_title' => '</span></div>',
    ));
}

if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => 'Before Footer',
        'id' => 'extra-widget',
        'description' => 'Before Footer',
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>'
    ));
}

// Place the widget before the header
add_filter ('before_navbar', 'add_my_widget');
function add_my_widget() {
if (function_exists('dynamic_sidebar')) {
dynamic_sidebar('Extra Widget Before Header');
}
}

/*** SHOP ***/

function register_shop_scripts() {

  if (is_page_template('shop.php')) {

    wp_enqueue_script('jquery');

    wp_register_script(
      'masonry',
      THEMEPATH . '/js/masonry.pkgd.min.js',
      array('jquery')
    );
    wp_enqueue_script('masonry');

    wp_register_script(
      'shop',
      THEMEPATH . '/js/shop.js',
      array('jquery', 'masonry')
    );
    wp_enqueue_script('shop');
  }
}
add_action('wp_enqueue_scripts', 'register_shop_scripts');

function build_url_with_params($url, $params) {
  $hasParams = strpos($url, '?') != false;

  foreach($params as $param => $value) {

    if (!$hasParams) {
      $url .= "?$param=$value";
      $hasParams = true;
    } else {
      $url .= "&$param=$value";
    }

  }

  return $url;
}

/*** LOOKBOOK ***/
add_action( 'after_setup_theme', 'lookbook_theme_setup' );
function lookbook_theme_setup() {
  add_image_size( 'lookbook', 233, 350, array('center', 'center') ); // (cropped)
}

/*** Author Excerpt Description ***/

function author_excerpt ($ID){
    $word_limit = 20; // Limit the number of words
    $more_txt = 'read more'; // The read more text
    $txt_end = '...'; // Display text end
    $authorName = get_the_author();
    $authorUrl = get_author_posts_url( $ID );
    // $authorUrl = 'http://adventurself.com/author/lady/';
    $authorDescriptionShort = wp_trim_words(strip_tags(get_the_author_meta('description', $ID)), $word_limit, $txt_end.'<br /> <a href="'.$authorUrl.'">'.$more_txt.'</a>');
    return $authorDescriptionShort;
}


/*** NAVBAR NEWSLETTER ***/
// function add_last_nav_item($items) {

//     $newsletter_html = file_get_contents(get_template_directory() . '/newsletter.php');

//     return $items .= '<li class="menu-item menu-item-has-children menu-newsletter">
//         <a href="#">Newsletter</a>
//         <ul class="sub-menu">
//             <li class="menu-item">'.
//             $newsletter_html.
//             '</li>
//         </ul>
//       </li>';
// }
// add_filter('wp_nav_menu_items','add_last_nav_item');

/***** NEWSLETTER POPUP *****/

function register_newsletter() {
        wp_enqueue_script('jquery');

        wp_register_script(
          'newsletter',
           THEMEPATH . '/js/newsletter.js',
           array('jquery')
        );
        wp_enqueue_script('newsletter');

}
add_action('wp_enqueue_scripts', 'register_newsletter');

/*** To enable easy pinterest sharing ***/
function first_image() {
  global $post, $posts;
  $first_img = '';
  ob_start();
  ob_end_clean();
  $output = preg_match_all('/<img.+src=[\'"]([^\'"]+)[\'"].*>/i', $post->post_content, $matches);
  $first_img = $matches[1][0];

  if(empty($first_img)) {
    $first_img = THEMEPATH . '/img/logo.png';
  }
  return $first_img;
}
// for variable sizes depending on how many posts
function get_map_dot_size($count){
  $b = 0.5;
  $a = 5;
  $size = 5 + ($a * log( $b * $count ));
  if ($size > 40){
    $size = 40;
  }
  return $size;
}


function isMobile() {
    return preg_match("/(android|avantgo|blackberry|bolt|boost|cricket|docomo|fone|hiptop|mini|mobi|palm|phone|pie|tablet|up\.browser|up\.link|webos|wos)/i", $_SERVER["HTTP_USER_AGENT"]);
}

function get_time_to_read( $post_id ) {

  $content_post = get_post($post_id);
  $content = $content_post->post_content;
  $word_count = str_word_count( $content );

  $words_per_minute = 250;
  $time_in_minutes = $word_count/$words_per_minute;

  if ($time_in_minutes < 1){
    $time_in_minutes = 'under 1 min';
  } else {

    $time_in_minutes = '<span>over '.floor($time_in_minutes).' min</span>';
  }

  return $time_in_minutes;

}


function get_related_author_posts() {
    global $authordata, $post;
    $authors_posts = get_posts( array( 'author' => $authordata->ID, 'post__not_in' => array( $post->ID ), 'posts_per_page' => 3 ) );
    $output = '';
    foreach ( $authors_posts as $authors_post ) {
      $post_thumbnail = get_the_post_thumbnail($authors_post->ID );
      $output .= '<div class="author-sidebar-image">'.$post_thumbnail.'</div>';

      $thumb_url = wp_get_attachment_image_src($authors_post->ID,'thumbnail', true);

      $output .= '<a style="background-image: url(' . $thumb_url[0] . ')" class="author-sidebar-text" href="' . get_permalink( $authors_post->ID ) . '">' . apply_filters( 'the_title', $authors_post->post_title, $authors_post->ID ) . '</a>';
    }

    return $output;
}
