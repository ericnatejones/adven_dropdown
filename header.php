<!doctype html>
<html>
<head>
    <title><?php bloginfo('name'); ?> <?php wp_title(); ?></title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <meta name="viewport" content="width=device-width">

    <!--[if lt IE 9]> <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script> <![endif]-->

    <script>
      (function(d) {
        var config = {
          kitId: 'tce1sbl',
          scriptTimeout: 3000
        },
        h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='//use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    </script>

    <link rel="icon" type="image/png" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png?v=1" />

    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" />
    <link rel="pingback" href="<?php bloginfo('pinback_url'); ?>" />

     <!--  Adobe Typekit -->
    <script src="https://use.typekit.net/mzf0bpr.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">

    <?php wp_head(); ?>

</head>

<body class="<?php echo join(' ', get_body_class()); ?>">

    <header id="header" class="large">
        <div id="logo-wrapper">

            <a href="<?php echo home_url(); ?>"><img id="header-logo" src="<?php echo IMAGES; ?>/logo.png" style="display: inline;"></a>
            <a class="small-logo" href="<?php echo home_url(); ?>"><img id="header-logo-small" src="<?php echo IMAGES; ?>/logo-small.png" style="display: none;"></a>

        </div>
        <div id="nav-wrapper" class='main-nav wrapper'>
            <i class="fa fa-navicon" id="nav-icon"></i>
            <div id="menu-hide">
                <?php
                    $defaults = array(
                        'container'       => 'nav',
                        'container_id'    => 'main-menu-container',
                        'menu_id'         => 'main-menu',
                        'theme_location'  => 'main'
                    );

                    wp_nav_menu( $defaults );
                ?>



                <div class="sub-nav" style="width: 1134px; display: none; visibility: visible;">

                    <div id='box-1' class="box">
                      <div class="item sub-categories">

                          <?php
                          $args = array(
                              'style' => 'list'
                          );
                          wp_list_categories(); ?>
                      </div>
                    </div>
                    <div id='box-2' class="box">
                      <?php
                      if ( have_posts() ) : while ( have_posts() ) : the_post();

                      ?><h2><?php the_title(); ?></h2>
                              <?php if ( has_post_thumbnail() ) {?>
                              <div class=" image-wrapper-for-dropdown">

                                         <?php
                                      if (has_post_video( get_the_id() )){

                                          the_post_video();

                                      } else { ?>


                                          <a class="dropdown-image" href="<?php the_permalink(); ?>" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
                                          </a>
                                          <?php
                                          do_action('read_later');

                                          }  ?>
                              </div>
                              <?php } ?>
                              <div class="post-body">
                                  <?php echo substr(get_the_excerpt(), 0, 238); ?>...
                                  <a class="read-more" href="<?php the_permalink(); ?>">Continue Reading</a>
                              </div>



                          <?php break; endwhile; else: ?>
                          <p class="no-posts"><?php _e('No posts were found. Sorry!'); ?></p>
                          <?php endif; ?>

                    </div>
                    <div id='box-3' class="box">

                      <?php
                      $counter = 0;


                      if ( have_posts() ) : while ( have_posts() ) : the_post();
                      $counter = $counter + 1;

                      if ($counter == 4){
                        wp_reset_postdata();
                        break;
                      }

                      ?>
                          <article class="post dropdown-post">

                              <div class="dropdown-post-body">
                                <h3>
                                  <?php
                                  the_title();
                                  ?></h3><?php
                                  echo substr(get_the_excerpt(), 0, 30); ?>...
                                  <a class="read-more" href="<?php the_permalink(); ?>">Continue Reading</a>
                              </div>
                              <div class=" loop-image-wrapper-for-dropdown">
                                <?php the_post_thumbnail('thumbnail') ?>

                              </div>


                          </article>

                          <?php endwhile; else: ?>
                          <p class="no-posts"><?php _e('No posts were found. Sorry!'); ?></p>
                          <?php endif; ?>

                    </div>
                </div>




            </div>
            <?php
            wp_reset_query();
            get_search_form();
            rewind_posts();
             ?>
        </div>

    <div id="social-wrapper">
            <?php if(strlen(get_option('twitter')) > 0): ?><a target="_blank" href="<?php echo get_option('twitter'); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?>
            <?php if(strlen(get_option('facebook')) > 0): ?><a target="_blank" href="<?php echo get_option('facebook'); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?>
            <?php if(strlen(get_option('pinterest')) > 0): ?><a target="_blank" href="<?php echo get_option('pinterest'); ?>"><i class="fa fa-pinterest"></i></a><?php endif; ?>
            <?php if(strlen(get_option('instagram')) > 0): ?><a target="_blank" href="<?php echo get_option('instagram'); ?>"><i class="fa fa-instagram"></i></a><?php endif; ?>
            <?php if(strlen(get_option('youtube')) > 0): ?><a target="_blank" href="<?php echo get_option('youtube'); ?>"><i class="fa fa-youtube"></i></a><?php endif; ?>
            <?php if(strlen(get_option('bloglovin')) > 0): ?><a target="_blank" href="<?php echo get_option('bloglovin'); ?>"><i class="fa fa-heart"></i></a><?php endif; ?>
        </div>
    </header>

    <?php
    $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;

    if (is_front_page() && 1 == $paged){
        $args = array( 'category_name' => 'featured', 'posts_per_page' => 50);
        $loop = new WP_Query( $args );

        if ( $loop->have_posts() ) : ?>



            <section id="slideshow">
            <!-- <section class="slider"> -->
            <div class="previous" type="image" style="background-image: url(<?php echo IMAGES; ?>/arrow-back.png)"></div>
            <div class="next" type="image" style="background-image: url(<?php echo IMAGES; ?>/arrow-forward.png)"></div>

                <?php

                while ( $loop->have_posts() ) : $loop->the_post();
                $feat_image_url = get_post_meta( get_the_id(), 'image_for_featured_slider_featured-image-for-slider', true );
                $feat_image_title_color = get_post_meta( get_the_id(), 'featured_slider_options_color_color', true);
                $feat_image_title_position = get_post_meta( get_the_id(), 'featured_slider_options_position_text_position_', true);
                $feat_image_title_color_class = '';
                $feat_image_title_position_class = '';
                if ($feat_image_title_color == 'Black'){
                    $feat_image_title_color_class = 'black ';
                }
                if ($feat_image_title_position == 'Left'){
                    $feat_image_title_position_class = 'left';
                } elseif ($feat_image_title_position == 'Right') {
                    $feat_image_title_position_class = 'right';
                }


                  ?>



                    <div class="featured-img" >
                      <?php if (strlen(trim($feat_image_url)) == 0) { ?>

                            <a href="<?php the_permalink();?>" class="img" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
                            </a>
                        <?php }else{
                        ?>


                            <a href="<?php the_permalink();?>" class="img" style="background-image: url(<?php echo $feat_image_url; ?>)">
                            </a>
                        <?php
                        }
?>
                        ?>
                        <div class="slideshow-title-wrapper <?php echo $feat_image_title_color_class.$feat_image_title_position_class; ?>">
                            <h2 class="slideshow-title"><a href="<?php the_permalink();?>"><?php the_title();?></a></h2>
                            <p class='slideshow-date'><?php echo substr(get_the_excerpt(), 0, 170); ?>...</p>
                            <a href="<?php the_permalink();?>" class="slideshow-button">Continue Reading</a>
                        </div>

                    </div>
                 <?php



              endwhile; ?>

            </section>
        <?php endif; ?>


    <?php
    }
    ?>


    <section id="content">
