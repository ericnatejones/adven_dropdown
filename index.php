        <?php get_header(); ?>

        <div class="container">
            <div class="row">

                <div id="main-content" class="col-sm-12">
                    <h1 id="page-title"> Search </h1>

                    <?php include "loop.php"; ?>

                    <?php
                    $prev_link = get_previous_posts_link('Previous');
                    $next_link = get_next_posts_link('Next');
                    ?>

                    <div class="pagination-links">

                     <?php echo $prev_link; ?>
                     <?php if ($prev_link && $next_link): ?>&nbsp;&nbsp;/&nbsp;<?php endif; ?>
                     <?php echo $next_link; ?>

                     </div>

                </div>

            </div>
        </div>

        <?php get_footer(); ?>
