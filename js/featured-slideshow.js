jQuery(document).ready(function($){
// .get here


    /* reverse order of images so slideshow is in right order */
    var slideshow = $('#slideshow');
    var images = slideshow.children('.featured-img').not('#loader');


    $('#slideshow .next').click(function(){
      next(slideshow);
    });

    $('#slideshow .previous').click(function(){
      previous(slideshow);
    });
    //images.hide();

    $('#slideshow').imagesLoaded().done(function(){
        $('#loader').remove();
        slideshow.append(images.get().reverse());
        images.show();
        resize_slideshow(images);
        if (slideshow.children('.featured-img').length > 1) {
            setInterval(function(){
                next(slideshow);
            }, 5000);
        }
    });

    /* fix weirdness on resize */

    $(window).resize(function() {
        resize_slideshow(images);
    });

});

var next = function(slideshow) {
  slideshow.children('.featured-img').last().fadeOut('slow', function(){
      jQuery(this).remove().prependTo(slideshow).show();
  });
}

var previous = function(slideshow) {

  slideshow.children('.featured-img').first().hide().appendTo(slideshow).fadeIn();
}

var resize_slideshow = function(images){
    var max_height = -1;
    images.each(function(){
        if (jQuery(this).attr('id') != 'loader'){
            if (max_height == -1) {
                max_height = jQuery(this).height();
            } else if (max_height > jQuery(this).height()){
                max_height = jQuery(this).height();
            }
        }
    });

    if (max_height) {
        jQuery('#slideshow').css({'max-height': max_height+'px'});
    }

}
