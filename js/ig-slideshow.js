jQuery(document).ready(function($){

    var instagram_pics = $('.instagram-pics li');
    instagram_pics.hide();
    instagram_pics.first().remove().appendTo('.instagram-pics').show();

    setInterval(function(){
        $('.instagram-pics li').first().hide().remove().appendTo('.instagram-pics').fadeIn('slow');

    }, 5000);
});
