jQuery(document).ready(function($){
	$('#menu-hide').addClass('mobile-hide');

	$('#nav-icon').click(function(){
		$('#menu-hide').toggleClass('mobile-hide');
		$('#nav-icon').toggleClass('nav-icon-pop-out');
		$('#nav-icon').toggleClass('fa-navicon');
		$('#nav-icon').toggleClass('fa-times');
	});

    /*** header resize on scroll ***/

    //cache to minimze dom traversal
    var $slideshow = $('#slideshow');
    var $header = $('#header');
    var $doc = $(document);

    tryResize($slideshow, $header, $doc);

    $(window).scroll(function(e){
        tryResize($slideshow, $header, $doc);
    });

    function tryResize($slideshow, $header, $doc){
        var scrollPosition = $doc.scrollTop();
        var height = $header.height();

        if (scrollPosition >= height) {
            $header.removeClass('large');
            $('#header-logo').hide();
            $('#header-logo-small').fadeIn();

        } else {
            $header.addClass('large');
            $('#header-logo').fadeIn();
            $('#header-logo-small').hide();

        }
    }

    // Mobile Navigation Stuff
    $('#main-menu a').click(function(){
        var href = $(this).attr('href');

        if (href == '#') {
            $(this).siblings('.sub-menu').toggleClass('mobile-open');
        }
    });

	$('.menu-newsletter input[type="email"]').focus(function(){
		$(this).parents('.sub-menu').addClass('stay-open');
	});

	$('.menu-newsletter input[type="email"]').blur(function(){
		$(this).parents('.sub-menu').removeClass('stay-open');
	});


    // Scroll top
    $(window).scroll(function(){
        if ($(this).scrollTop() > 100) {
            $('.scrollToTop').fadeIn();
        } else {
            $('.scrollToTop').fadeOut();
        }
    });

    //Click event to scroll to top
    $('.scrollToTop').click(function(){
        $('html, body').animate({scrollTop : 0},800);
        return false;
    });

		// sub nav

		$('.menu-item, .sub-nav').on({
			  mouseenter: function() {
					$('.sub-nav').css('display','block')
			  },
			  mouseleave: function() {
					$('.sub-nav').css('display','none')
			  }
			});


});
