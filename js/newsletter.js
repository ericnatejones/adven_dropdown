jQuery(document).ready(function ($) {

    setTimeout(function(){
        if (getCookie('kpi_newsletter_shown') !== '1') {
            document.cookie="kpi_newsletter_shown=1";
            $('#newsletter-modal').fadeIn();
        }
    }, 10000);

    $('#newsletter-close').click(function(){
        $('#newsletter-modal').fadeOut();
    });

    $('.subscribe-modal-link').click(function(e) {
        e.preventDefault();
        $('#newsletter-modal').fadeIn();
    })

    $('.modal').click(function() {
        $('.modal').fadeOut();
    });

    $('.modal-box').click(function(e) {
        e.preventDefault();
        e.stopPropagation();
    });
    
    $('.modal-box .social-newsletter a').click(function(e) {
        e.stopPropagation();
    });
    
    $('.modal-box input[type="submit"]').click(function(e) {
        e.stopPropagation();
    });
});

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for(var i=0; i<ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length,c.length);
    }
    return "";
}
