jQuery(document).ready(function ($) {
  $('.grid img').load(function () {
    $('.grid').masonry({
      itemSelector: '.shop-item',
      percentPosition: true,
      isAnimated: false
    })
  })
  $('.grid').masonry({
    itemSelector: '.shop-item',
    percentPosition: true
  })
})
