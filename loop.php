<?php 

if ( have_posts() ) : while ( have_posts() ) : the_post();
?>
    <article class="post loop-post">
        <?php if ( has_post_thumbnail() ) {?>
        <div class=" loop-image-wrapper col-sm-6">

                   <?php
                if (has_post_video( get_the_id() )){

                    the_post_video();

                } else { ?>


                    <a class="loop-image" href="<?php the_permalink(); ?>" target="blank" class="select-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php the_post_thumbnail_url( 'large' ); ?>)">
                    </a>
                    <?php
                    do_action('read_later');

                    }  ?>
        </div>
        <?php } ?>

        <div class="loop-text col-sm-6">
            <header>
                <span class="categories"><?php display_categorys_except_featured() ?></span>
                <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                <div class="date-author"><?php the_time('F j, Y'); ?>   •    <?php the_author_link(); ?></div>
            </header>
            <div class="post-body">
                <?php echo substr(get_the_excerpt(), 0, 100); ?>...
                <a class="read-more" href="<?php the_permalink(); ?>">Continue Reading</a>
            </div>
            <footer>
                <span class="time-span"><i class="fa fa-clock-o"></i><?php echo get_time_to_read(get_the_ID()); ?></span>
                <span class="share">
                    <p> Share: </p>
                        <a target="popup" onclick="window.open('http://facebook.com/sharer/sharer.php?u=<?php echo urlencode(get_permalink()); ?>','name','width=600,height=400')" href="#"><i class="fa fa-facebook"></i></a>
                        <a target="popup" onclick="window.open('http://twitter.com/share?url=<?php echo urlencode(get_permalink()); ?>','name','width=600,height=400')" href="#"><i class="fa fa-twitter"></i></a>
                        <a target="popup" onclick="window.open('//www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&media=<?php echo urlencode(first_image()); ?>&description=<?php echo urlencode( html_entity_decode(get_the_title()) . ' | The Glam & Glitter'); ?>','name','width=600,height=400')" data-pin-do="skipLink" href="#"><i class="fa fa-pinterest-p"></i></a>
                </span>
            </footer>
        </div>

    </article>

    <?php endwhile; else: ?>
    <p class="no-posts"><?php _e('No posts were found. Sorry!'); ?></p>
    <?php endif; ?>
