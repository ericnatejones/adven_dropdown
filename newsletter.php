<div class="fancy-input email">
    <div class="fancy-input-inner">
        <!-- Begin MailChimp Signup Form -->
        <div id="mc_embed_signup">
            <form action="//streetinspired.us10.list-manage.com/subscribe/post?u=3214a23ae21977166c8a38462&amp;id=e4a27a5e0f" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

            <div class="mc-field-group">
                <input type="email" value="" name="EMAIL" class="required email" placeholder="Subscribe by Email" id="mce-EMAIL">
                <input type="submit" class="button">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;"><input type="text" name="b_3214a23ae21977166c8a38462_e4a27a5e0f" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
            </form>
        </div>

        <!--End mc_embed_signup-->
    </div>
</div>
