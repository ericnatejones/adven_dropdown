<?php
/*
Template Name Posts: No Sidebar
*/
?>
            <?php get_header(); ?>

        <div class="page-container">
            <div class="row">

                <div id="main-content" class="col-sm-12">

                    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

                    <article class="post single">
                         <header>
                            <span class="categories"><?php display_categorys_except_featured()?>

                            </span>
                            <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
                            <div class="date-author"><?php the_date(); ?>   •    <?php the_author_link(); ?></div>
                            <span class="time-span"><i class="fa fa-clock-o"></i><?php echo do_shortcode('[time-span]'); ?></span>
                        </header>
                        <div class="post-body">
                            <div class="the-content">

                                   <?php
                                if (has_post_video( get_the_id() )){ ?>
                                    <div class="content-featured-video"> 
                                        <?php the_post_video(); ?>
                                    </div>
                               <?php } else { ?>

                                    <?php the_post_thumbnail( 'large' ); ?>

                                    <?php }

                                    do_action('read_later'); ?>

     
                                <div class="content-column">
                                  <div class="contributors-column">
                                      <?php
                                          $images_title_arr = get_post_custom_values('images_title');
                                          $images_title = $images_title_arr[0];

                                          $images_1_arr = get_post_custom_values('images_1');
                                          $images_1_text = $images_1_arr[0];

                                          $images_2_arr = get_post_custom_values('images_2');
                                          $images_2_text = $images_2_arr[0];
                                      ?>
                                      <div class="contributors">
                                          <h4>Contributors</h4>
                                          <p class="subtitle">Copy By</p>
                                          <p><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></p>
                                          <br />
                                          <p class="subtitle"><?php echo $images_title; ?></p>
                                          <p><?php echo $images_1_text; ?></p>
                                          <p><?php echo $images_2_text; ?></p>
                                      </div>
                                      <div class="share-column">
                                          <h4>Share This</h4>
                                              <a target="#"><i class="fa fa-comment"></i><div class="share-text">Comment</div></a>
                                              <a target="_blank" href="https://www.facebook.com/Adventurself-1615773362034293/"><i class="fa fa-facebook"></i><div class="share-text">Like</div></a>
                                              <a target="popup" onclick="window.open('//www.pinterest.com/pin/create/button/?url=<?php echo urlencode(get_permalink()); ?>&media=<?php echo urlencode(first_image()); ?>&description=<?php echo urlencode( html_entity_decode(get_the_title())); ?>','name','width=600,height=400')" data-pin-do="skipLink" href="#"><i class="fa fa-pinterest-p"></i><div class="share-text">Pin</div></a>
                                              <a target="popup" onclick="window.open('http://twitter.com/share?url=<?php echo urlencode(get_permalink()); ?>','name','width=600,height=400')" href="#"><i class="fa fa-twitter"></i><div class="share-text">Tweet</div></a>
                                      </div>
                                  </div>
                                    <?php the_content(); ?>
                                </div>
                            </div>

                        </div>
                        <div class="tags"><?php the_tags( 'Tags: ', ', ', '<br />' ); ?> </div>
                        <div class="author-box">
                            <span class="author-image"><?php echo get_avatar( get_the_author_meta( 'ID' ), 150 ); ?></span>
                            <div class="author-text">
                                <h2 class="author-name"><?php the_author_firstname(); ?> <?php the_author_lastname(); ?></h2>
                                <p class="author-social">
                                    <?php echo $curauth->facebook; ?><a target="_blank" href="http://www.facebook.com/<?php echo $curauth->facebook; ?>"><i class="fa fa-facebook"></i></a>
                                    <?php echo $curauth->instagram; ?><a target="_blank" href="http://www.instagram.com/<?php echo $curauth->instagram; ?>"><i class="fa fa-instagram"></i></a>
                                    <?php echo $curauth->twitter; ?><?php echo the_author_meta(‘twitter’); ?><a target="_blank" href="http://www.twitter.com/<?php echo $curauth->twitter; ?>"><i class="fa fa-twitter"></i></a>
                                <p class="author-description"><?php the_author_description(); ?></p>
                            </div>
                        </div>
                        <?php if (function_exists('related_posts')): ?>
                        <div class="related">
                            <div class="related-title">
                              <h2>Related Articles</h2>
                            </div>
                            <?php related_posts(); ?>
                        </div>
                        <?php endif; ?>

                    </article>



                    <section id="comments">

                    <?php comments_template(); ?>

                    </section>


                    <?php endwhile; else: ?>
                    <p><?php _e('No posts were found. Sorry!'); ?></p>
                    <?php endif; ?>

                </div>

            </div>

            <div class="pagination-wrapper">
            <?php if(get_previous_post()) {
               $previous_post_url = get_permalink(get_previous_post()->ID);
               ?>
               <a class="pagination-links-single pagination-prev" href='<?php echo $previous_post_url; ?>'>
                   <div class="pagination-image"><?php echo get_the_post_thumbnail(get_previous_post()->ID, 'medium'); ?></div>
                   <?php var_dump(get_previous_post()); ?>
                   <div class="pagination-text">
                       <div class="pagination-subtitle">Previously</div>
                       <div class="pagination-title"><?php echo get_previous_post()->post_title; ?></div>
                   </div>
               </a>
             <?php } else {
               $previous_post_url = '#';
             }

             if(get_next_post()) {
               $next_post_url = get_permalink(get_next_post()->ID);
               ?>
               <a class="pagination-links-single pagination-next" href='<?php echo $next_post_url; ?>'>
                   <div class="pagination-image"><?php echo get_the_post_thumbnail(get_next_post()->ID, 'medium'); ?></div>
                   <div class="pagination-text">
                       <div class="pagination-subtitle">Next Up</div>
                       <div class="pagination-title"><?php echo get_next_post()->post_title; ?></div>
                   </div>
               </a>
             <?php } else {
                $previous_post_url = '#';
              }?>
              </div>


        </div>

        <?php get_footer(); ?>

