<?php
/*
* Template Name: Archives
*/
?>
<?php get_header(); ?>
<div class="container">
    <div class="row">

        <div id="main-content" class="col-sm-8">

            <h2>Archives by Month:</h2>
            <ul>
                <?php wp_get_archives('type=monthly'); ?>
            </ul>

            <h2>Archives by Subject:</h2>
            <ul>
                <?php wp_list_categories('hierarchical=0&title_li='); ?>
            </ul>

        </div>

        <?php get_sidebar(); ?>
    </div>
</div>
<?php get_footer(); ?>