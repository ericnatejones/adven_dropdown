<div class="fancy-input search">
    <div class="fancy-input-inner <?php if (get_search_query()) : ?>searched<?php endif; ?>">
        <form action="/">
            <input type="text" name="s" placeholder="SEARCH" <?php if (get_search_query()) : ?>value="<?php the_search_query(); endif; ?>">
            <?php if (get_search_query()) : ?>
            	<a title="Clear Search Term" class="clear-query" href="<?php echo get_home_url(); ?>">
            	</a><?php endif; ?>
        </form>
    </div>
</div>