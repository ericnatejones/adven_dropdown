<?php
/**
 * Template Name: Shop Page
 *
 */

get_header(); ?>

 <div class="container">
     <div class="row">

         <div id="main-content" class="col-sm-8">

             <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

             <article class="post single">
                 <header>
                     <h1><?php the_title(); ?></h1>
                 </header>

                 <?php
                 $paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
                 $shop_query = new WP_Query(
                  array(
                    'fields' => 'ids',
                    'posts_per_page' => 2,
                    'paged' => $paged,
                    'meta_query' => array(
                      array(
                        'key' => 'img-thumbnail-src',
                        'compare' => 'EXISTS'
                      ),
                      array(
                        'key' => 'img-thumbnail-src',
                        'value' => 'a:1:{i:0;s:0:"";}',
                        'compare' => '!='
                      )
                    )
                  )
                );

                 ?> <div class="grid"> <?php

                 if ( $shop_query->have_posts() ) : foreach ( $shop_query->posts as $id ) :

                   $imgSrc = get_post_meta( $id, 'img-thumbnail-src', true );
                   $imgUrl = get_post_meta( $id, 'img-thumbnail-url', true );

                   for ($i = 0; $i < count($imgSrc); $i++) {
                     echo '<a target="_blank" class="shop-item" href="'.$imgUrl[$i].'"><img class="img-responsive" src="'.Post_Footer_Slider::get_image_size_from_url($imgSrc[$i], 'medium').'"></a>';
                   }

                 endforeach; endif; ?>

               </div>

             </article>

             <div class="pagination-links">
                    <?php if ($paged > 1) : ?><a href="<?php echo build_url_with_params(get_permalink(), array('paged'=>$paged-1)); ?>">Previous</a><?php endif; ?>
                    <?php if (($paged > 1) && ($paged < $shop_query->max_num_pages)): ?> &nbsp;&nbsp;/&nbsp;&nbsp;<?php endif; ?>
                    <?php if ($paged < $shop_query->max_num_pages) : ?><a href="<?php echo build_url_with_params(get_permalink(), array('paged'=>$paged+1)); ?>">Next</a><?php endif; ?>
             </div>

           <?php endwhile; endif; ?>

         </div>

         <?php get_sidebar(); ?>

     </div>
 </div>

 <?php get_footer(); ?>
