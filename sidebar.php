<aside id="sidebar">

    <div class="widget social">
        <div class="widget-header">
            <span>Socialize</span>
        </div>
        <ul>
            <li><?php if(strlen(get_option('twitter')) > 0): ?><a target="_blank" href="<?php echo get_option('twitter'); ?>"><i class="fa fa-twitter"></i></a><?php endif; ?></li>
            <li><?php if(strlen(get_option('facebook')) > 0): ?><a target="_blank" href="<?php echo get_option('facebook'); ?>"><i class="fa fa-facebook"></i></a><?php endif; ?></li>
            <li><?php if(strlen(get_option('pinterest')) > 0): ?><a target="_blank" href="<?php echo get_option('pinterest'); ?>"><i class="fa fa-pinterest"></i></a><?php endif; ?></li>
            <li><?php if(strlen(get_option('instagram')) > 0): ?><a target="_blank" href="<?php echo get_option('instagram'); ?>"><i class="fa fa-instagram"></i></a><?php endif; ?></li>
            <li><?php if(strlen(get_option('bloglovin')) > 0): ?><a target="_blank" href="<?php echo get_option('bloglovin'); ?>"><i class="fa fa-heart"></i></a><?php endif; ?></li>
        </ul>
        <div class="clearfix"></div>
        <div class="social-subscription">
            <p class="subscription-text">
                Want to get all the cool perks that come with being an adventurer? Sign up now
            </p>
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
            <form action="//adventurself.us12.list-manage.com/subscribe/post?u=855c48b64509d4385c433558e&amp;id=024555831a" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <div id="mc_embed_signup_scroll">

            <div class="mc-field-group">
                <input type="email" placeholder="EMAIL ADDRESS" name="EMAIL" class="required email" id="mce-EMAIL">
            </div>
                <div id="mce-responses" class="clear">
                    <div class="response" id="mce-error-response" style="display:none"></div>
                    <div class="response" id="mce-success-response" style="display:none"></div>
                </div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
                <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_855c48b64509d4385c433558e_024555831a" tabindex="-1" value=""></div>
                <div class="clear"><input type="submit" value=">" name="subscribe" id="mc-embedded-subscribe" class="button"></div>
                </div>
            </form>
            </div>

            <!--End mc_embed_signup-->
        </div>
    </div>

    <?php if (is_front_page()): ?>
        <div class="widget featured-adventurers">
            <div class="widget-header">
                <span>Featured Adventurers</span>
            </div>
                <span class="author-image"><?php echo get_avatar( get_the_author_meta( 'ID', 11 ), 350 ); ?></span>
                <div class="author-text">
                    <h2 class="author-name"><?php echo get_the_author_meta( first_name, 11 ); ?>  <?php echo get_the_author_meta( last_name, 11 ); ?></h2>
                    <p class="author-social">
                        <a target="_blank" href="http://www.facebook.com/<?php echo get_the_author_meta( facebook, 11 ); ?>"><i class="fa fa-facebook"></i>/<?php echo get_the_author_meta( facebook, 11 ); ?></a>
                        <a target="_blank" href="http://www.instagram.com/<?php echo get_the_author_meta( instagram, 11 ); ?>"><i class="fa fa-instagram"></i>@<?php echo get_the_author_meta( instagram, 11 ); ?></a>
                        <a target="_blank" href="http://www.twitter.com/<?php echo get_the_author_meta( twitter, 11 ); ?>"><i class="fa fa-twitter"></i>@<?php echo get_the_author_meta( twitter, 11 ); ?></a>
                    </p>
                </div>
                <p class="author-description"><?php if (function_exists('author_excerpt')){echo author_excerpt(11);} ?></p>
        </div>

        <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Front Page Sidebar')) : ?>
        <?php endif; ?>
    <?php endif; ?>

   <?php if( is_single($post)) : ?>
        <div class="widget related-sidebar">
            <div class="widget-header">
                <span>Follow This Author</span>
            </div>
            <div id='other-posts-by-this-author'>
              <?php echo get_related_author_posts(); ?>
            </div>
          </div>
    <?php endif; ?>

    <?php if ( !function_exists( 'dynamic_sidebar' ) || !dynamic_sidebar('Primary Sidebar')) : ?>
        <?php endif; ?>

</aside>
