<?php
if( isset($_POST['did_save']) && $_POST['did_save'] == 'Y') {
    update_option( 'pinterest', $_POST['pinterest']);
    update_option( 'facebook', $_POST['facebook']);
    update_option( 'instagram', $_POST['instagram']);
    update_option( 'twitter', $_POST['twitter']);
    update_option( 'youtube', $_POST['youtube']);
    update_option( 'bloglovin', $_POST['bloglovin']);
    echo "<div class='updated'><p><strong>Settings Saved</strong></p></div>";
  }

  $pinterest_url = get_option( 'pinterest' );
  $facebook_url = get_option( 'facebook' );
  $twitter_url = get_option( 'twitter' );
  $instagram_url = get_option( 'instagram' );
  $youtube_url   = get_option( 'YouTube' );
  $bloglovin_url = get_option( 'bloglovin' );


  ?>
  <div class="wrap">
    <style>
      label {
        width: 100px;
        display: inline-block;
      }
    </style>
    <h2><?php _e('Social Links', 'social-links'); ?></h2>
    <form name="social-form" method="post" action="">
      <input type="hidden" name="did_save" value="Y">

      <p><label><?php _e("Twitter URL:", 'social-links' ); ?></label>
      <input type="text" name="twitter" value="<?php echo $twitter_url; ?>" size="50">
      </p><hr />

      <p><label><?php _e("Facebook URL:", 'social-links' ); ?></label>
      <input type="text" name="facebook" value="<?php echo $facebook_url; ?>" size="50">
      </p><hr />

      <p><label><?php _e("Pinterest URL:", 'social-links' ); ?></label>
      <input type="text" name="pinterest" value="<?php echo $pinterest_url; ?>" size="50">
      </p><hr />

      <p><label><?php _e("Instagram URL:", 'social-links' ); ?></label>
      <input type="text" name="instagram" value="<?php echo $instagram_url; ?>" size="50">
      </p><hr />

      <p><label><?php _e("YouTube URL:", 'social-links' ); ?></label>
      <input type="text" name="youtube" value="<?php echo $youtube_url; ?>" size="50">
      </p><hr />

      <p><label><?php _e("Bloglovin URL:", 'social-links' ); ?></label>
      <input type="text" name="bloglovin" value="<?php echo $bloglovin_url; ?>" size="50">
      </p><hr />

      <input type="submit" value="Save Changes">
    </form>
  </div>