<?php get_header(); ?>
<div class='mapcontainer'>
  <div class='map'>

  </div>
</div>

<script>


jQuery(document).ready(function($){
  var screenWidth = window.innerWidth
  console.log(screenWidth);
  if (screenWidth < 500){
    var zoomLevel = 10
  } else if (screenWidth < 750) {
    var zoomLevel = 8
  } else {
    var zoomLevel = 1
  }
	$(".mapcontainer").mapael({
	    map : {
	        name : "world_countries",
          defaultArea : {
            attrs : {
              fill: "#dddddd",
              stroke: "#dddddd"
            },
            attrsHover : {
              fill: "#dddddd"
            }
          },
          defaultPlot : {
            attrs : {
              fill: '#000000'
            },
            text : {
              attrs : {
                fill: "#333333",
                'font-size' : 12
              }
            }
          },
          zoom: {
            enabled: true,
            mousewheel: false,
            maxLevel : 10,
              init: {
                latitude: 54.717079,
                longitude: 25.00116,
                level:zoomLevel

              }

          }
      },

      plots : {
        <?php
          $locations = get_terms('locations');
          foreach ($locations as $location) {

            $locations_term = get_term_meta( $location->term_id, 'latlong', true );

            $latlng = explode(',', $locations_term);
            if (!empty($latlng[0])){


            ?>

            '<?php echo $location->slug; ?>' : {
              type: 'circle',
              latitude : <?php echo $latlng[0]; ?>,
              longitude : <?php echo $latlng[1]; ?>,
              tooltip: {content : "<?php echo $location->name; ?>"},
              href: '<?php echo get_term_link($location); ?>#main-content',
              size: 6.5
            }<?php
              if ($location !== end($locations)){
                echo ',';
              }
            ?>

        <?php  }}

        ?>
      }
  });
})
</script>
<div id='location-list'><p>

  <?php
  $my_locations = get_terms( 'locations', 'orderby=name&hide_empty=0' );


  if (isMobile()){
    $num_of_col = 2;
  }


  //display locations

  foreach($my_locations as $location){


    echo '<a href="'.get_term_link($location).'#main-content">'.$location->name.'</a><br>';

  }
  ?></p>
</div>
<div class="category-container">
    <div class="row">

        <div id="main-content" class="col-sm-12 lookbook">

          <h1 id="page-title" class="category-title travel-title"> <?php
            $queried_object = get_queried_object();
            echo $queried_object->name;
           ?></h1>

          <?php include "category-loop.php"; ?>

          <div class="pagination-links">
            <?php the_posts_pagination( array( 'mid_size' => 3 ) ); ?>
          </div>

        </div>
    </div>
</div>
    <?php get_footer(); ?>
