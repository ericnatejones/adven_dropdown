<?php
/*
YARPP Template: Verisage YARPP Sidebar
Author: Verisage
Description: A related posts template
*/
?>
<div class="related-posts-wrapper">
  <?php if (have_posts()):?>
    <?php while (have_posts()) : the_post(); ?>
      <div class="related-article sidebar-yarpp">
        <div class="related-image">
          <?php $feat_image_url = wp_get_attachment_url( get_post_thumbnail_id() );?>

          <div class="featured-background-image" title="<?php the_title_attribute(); ?>" style="background-image: url(<?php echo $feat_image_url; ?>)">
          </div>
        </div>

        <h1><?php the_title(); ?></h1>

      </div>
    <?php endwhile; ?>
  <?php else: ?>
    <p>No related posts.</p>
  <?php endif; ?>
</div>
<!-- 768 to 970 3 posts instead of 4 -->
